<?php
session_start();
require "./../src/Conexion.php";
require "./../src/Jugador.php";
  $j=new Usuario();
  $j->conectar();
  $lista=$j->listarJugadores();
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
  <link rel="stylesheet" href="css/menu.css">          
  </head>
  <body>
  <?php include "./assets/navSup.php"; ?>
    <style>
      table {
        border-collapse: collapse;
        width: 100%;
      }

      tr, td {
        text-align: auto;
        padding: 8px;
      }

      tr:nth-child(even) {background-color: #f2f2f2;}
    </style>
  <table id="Lista">
    <br>
    <tr>
        <td><b>ID</b></td>
        <td><b>Nombre</b></td>
        <td><b>Apellidos</b></td>
        <td><b>Edad</b></td>
        <td><b>Curso</b></td>
        <td><b>Puntuación</b></td>
    </tr>
    <?php
    foreach ($lista as $usuario1) {
      echo "<tr>";
      echo "<td>".$usuario1['id']."</td>";
          echo "<td>".$usuario1['nombre']."</td>";
          echo "<td>".$usuario1['apellidos']."</td>";
          echo "<td>".$usuario1['edad']."</td>";
          echo "<td>".$usuario1['curso']."</td>";
          echo "<td>".$usuario1['puntuacion']."</td>";
      echo "</tr>";
    } ?>
  </table>

<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

<?php include "./assets/footer.php"; ?>
  </body>
</html>
